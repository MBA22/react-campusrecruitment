module.exports = {
    entry: './app/app.jsx',
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    resolve: {
        root: __dirname,
        alias: {
            Main: 'app/components/Main.jsx',
            Login: 'app/components/Login.jsx',
            Studentform: 'app/components/StudentForm.jsx',
            Companyform: 'app/components/CompanyForm.jsx',
            Company: 'app/components/Company.jsx',
            Studentmain: 'app/components/StudentMain.jsx',
            Adminmain: 'app/components/AdminMain.jsx',
        },
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                },
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/
            }
        ]
    }
};
