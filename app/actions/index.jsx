var {myFirebase, refFirebase}=require('app/firebase/');
var store = require('./../store/configureStore').storeConfig();

export var setUidOnLogin = function (uid) {
    console.log("In Set uid action", uid);
    return {
        type: "LOGIN",
        uid: uid,
    };
};

export var logout = function () {
    console.log("In logout function");

    return {
        type: "LOGOUT",
    };
};

export var roleCarrier = function (role) {
    console.log('at role carrier', role);
    return {
        type: "LOGINCHECK",
        role: role,
    }
};

export var loginStart = function (email, password, role) {
    return function (dispatch, getState) {

        myFirebase.auth().signInWithEmailAndPassword(email, password).then(function (result) {

            console.log("Authentication worked", result);
            dispatch(setUidOnLogin(result.uid));
            if (role == 'admin') {
                if (email === 'admin@admin.com' && password === 'admin123') {
                    dispatch(roleCarrier(role))
                }
                else return;
            }
            dispatch(roleCarrier(role));
        }, function (error) {
            console.log("Authentication failed", error);
        });
    }

};

export var logoutStart = function () {
    return function (dispatch, getState) {
        return myFirebase.auth().signOut().then(function () {
            console.log("Log out successfull");
            dispatch(logout);
        });
    }
};

export var AddStudentToDatabase = function (signup, user) {

    console.log("SomeThingClicked", name);
    return function (dispatch, getState) {
        dispatch(loginOrSignUp(''));
        dispatch(roleCarrier('student'));
        myFirebase.auth().createUserWithEmailAndPassword(signup.email, signup.password).then(function (result) {
            console.log("Authentication worked", result);
            var state = getState();
            var id = result.uid;
            var userInfo = refFirebase.ref(`students/${id}`);
            userInfo.set(user);
            store.dispatch(roleCarrier('student'));
            return {
                type: "ADDED_USER",
                user: user,
            };

        }, function (error) {
            console.log("Authentication didnot worked", error);
        });
    };
};

export var AddCompanyToDatabase = function (signup, user) {
    console.log("SomeThingClicked", name);
    return function (dispatch, getState) {
        dispatch(loginOrSignUp(''));
        dispatch(roleCarrier('company'));
        myFirebase.auth().createUserWithEmailAndPassword(signup.email, signup.password).then(function (result) {
            console.log("Authentication worked", result);
            var state = getState();
            var id = result.uid;
            var userInfo = refFirebase.ref(`companies/${id}`);
            store.dispatch(roleCarrier('company'));
            userInfo.set(user);
            return {
                type: "ADDED_USER",
                user: user,
            };

        }, function (error) {
            console.log("Authentication didnot worked", error);
        });
    };
};

export var loginOrSignUp = function (isSinging) {
    return {
        type: "ISLOGINGORSIGNUP",
        isSinging: isSinging,
    };
};


//////////////////Fetching Data Actions///////////////////////////////

export var startFetching = function () {
    return {
        type: "STARTING_FETCHING",
    };
};
export var doneFetching = function () {
    return {
        type: "DONE_FETCHING",
    };
};
export var doneFetchingStudent = function (returnedObject) {
    return {
        type: "DONE_FETCHINGSTUDENT",
        student: returnedObject,
    };
};

export var doneFetchingCompany = function (returnedObject) {
    return {
        type: "DONE_FETCHINGCOMPANY",
        company: returnedObject,
    };
};

export var doneFetchingStudents = function (returnedObject) {
    return {
        type: "DONE_FETCHINGSTUDENTS",
        students: returnedObject,
    };
};
export var getStudents = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var state = getState();
        var id = state.loginsignupReducer.uid;
        var getItem = refFirebase.ref(`students/${id}`);
        getItem.on('value', function (snapshot) {
            dispatch(doneFetchingStudent(snapshot.val()));
        });
    };
};

export var doneFetchingCompaniesPosts = function (returnedObject) {
        return {
            type: "DONE_FETCHINGCOMPANIES",
            companies: returnedObject,
        };
    }
    ;
export var getCompaniesPosts = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var state = getState();
        var id = state.loginsignupReducer.uid;
        var getItem = refFirebase.ref(`companies`);
        getItem.on('value', function (snapshot) {
            dispatch(doneFetchingCompaniesPosts(snapshot.val()));
        });
    };
};

export var getDataForCompanies = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var state = getState();
        var id = state.loginsignupReducer.uid;
        var getCompany = refFirebase.ref(`companies/${id}`);
        getCompany.on('value', function (snapshot) {
            dispatch(doneFetchingCompany(snapshot.val()));
        });
        var getStudents = refFirebase.ref(`students`);
        getStudents.on('value', function (snapshot) {
            dispatch(doneFetchingStudents(snapshot.val()));
        });
    };
};

export var addedPost = function (post) {
    return {
        type: "ADDED_POST",
        post: post,
    };
}

export var addPost = function (post) {
    return function (dispatch, getState) {

        var state = getState();
        var id = state.loginsignupReducer.uid;
        dispatch(startFetching());
        var companyPost = refFirebase.ref(`companies/${id}/posts`);
        companyPost.push(post);
        dispatch(addedPost(post));
        dispatch(doneFetching());
    };
};

export var doneFetchingCompaniesforAdmin = function (companies) {
    return ({
        type: "DONE_FETCHINGCOMPANIES_ADMIN",
        companies: companies,
    });
}
export var getDataForAdmin = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());

        var getCompanies = refFirebase.ref(`companies/`);
        getCompanies.on('value', function (snapshot) {
            dispatch(doneFetchingCompaniesforAdmin(snapshot.val()));
        });
        var getStudents = refFirebase.ref(`students`);
        getStudents.on('value', function (snapshot) {
            dispatch(doneFetchingStudents(snapshot.val()));
        });
    };
};
export var deleted = function (item) {
    return {
        type: "ITEM_DELETED",
        item: item,
    };
}
export var deleteCompany = function (key) {
    return function (dispatch, getState) {
        var company = refFirebase.ref(`companies/${key}`);
        company.remove();
        dispatch(deleted(key));
    };
};

export var deleteStudent = function (key) {
    return function (dispatch, getState) {
        var student = refFirebase.ref(`students/${key}`);
        student.remove();
        dispatch(deleted(key));
    };
};