var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var Company = React.createClass({

    componentDidMount: function () {
        this.props.getDataForCompanies();
    },
    companyPosts: function () {
        if (this.props.currentProps.company.posts != null || this.props.currentProps.company.posts != undefined) {
            return null;
        }
        else return null;
    },
    addThePost: function () {
        console.log(this.props.currentProps.fetching);
        this.props.addPosts(this.refs.postInput.value);
        console.log(this.props.currentProps.fetching);
    },
    render: function () {

        if (this.props.currentProps.fetching) {
            return <h5>Loading.....</h5>;
        }
        else {
            console.log(this.props.currentProps);
            var students = this.props.currentProps.students;
            var posts = this.props.currentProps.company.posts;
            return (
                <div className="well">
                    <h1 className="text-center">Company Info</h1>
                    <h3>{this.props.currentProps.company.name}</h3>
                    <ul className="list-group">
                        <li className="list-group-item">Number:{this.props.currentProps.company.mobNo}</li>
                        <li className="list-group-item">Email:{this.props.currentProps.company.email}</li>
                        <li className="list-group-item">City:{this.props.currentProps.company.city}</li>
                        <input className="form-control" ref="postInput" type="text"/>
                        <button class onClick={this.addThePost}>Add Post</button>
                        <h4>Posts</h4>
                        {
                            Object.keys(posts).map(function (key) {
                                return (
                                    <li>{posts[key]}</li>
                                );
                            })}
                    </ul>

                    <div>
                        <h2>Students</h2>
                        <div className="row">
                            {
                                Object.keys(students).map(function (key) {
                                    return (
                                        <div className="well">
                                            <ul className="list-group" key={key}>
                                                <h3 key={key}>{students[key].name}</h3>
                                                <li className="list-group-item">Father
                                                    Name:{students[key].fatherName}</li>
                                                <li className="list-group-item">Email:{students[key].email}</li>
                                                <li className="list-group-item">Gender:{students[key].gender}</li>
                                                <li className="list-group-item">Gender:{students[key].gender}</li>
                                                <li className="list-group-item">DOB:{students[key].dob}</li>
                                                <li className="list-group-item">Degree:{students[key].course}</li>
                                                <li className="list-group-item">Address:{students[key].address}</li>
                                                <li className="list-group-item">Postal Address:{students[key].studentPostalAddress}</li>
                                                <li className="list-group-item">City:{students[key].city}</li>
                                            </ul>
                                        </div>
                                    );
                                }.bind(this))
                            }
                        </div>
                    </div>
                </div>
            );
        }
    },
});


function mapStateToProps(state) {

    return {
        currentProps: state.getStudentsCompanies,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDataForCompanies: actions.getDataForCompanies,
        startFetching: actions.startFetching,
        addPosts: actions.addPost,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Company);