var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var studentInfo = {};
var signUp = {};
var StudentSignupForm = React.createClass({
    setGender: function (e) {
        var gender = e.target.value;
        studentInfo.gender = gender;
    },
    setCity: function (e) {
        var city = e.target.value;
        console.log(city);
        studentInfo.city = city;
    },
    setCourse: function (e) {
        var course = e.target.value;
        studentInfo.course = course;
    },

    handelValues: function (e) {
        e.preventDefault();
        studentInfo.name = this.refs.studentName.value;
        studentInfo.email = this.refs.studentEmail.value;
        signUp.email = this.refs.studentEmail.value;
        signUp.password = this.refs.studentPass.value;
        studentInfo.fatherName = this.refs.studentFatherName.value;
        studentInfo.studentPostalAddress = this.refs.studentPostalAddress.value;
        studentInfo.studentAddress = this.refs.studentAddress.value;
        studentInfo.cgpa = this.refs.cgpa.value;
        studentInfo.dob = this.refs.dob.value;
        studentInfo.mobNo = this.refs.mobNo.value;

        this.props.AddStudentToFirebase(signUp, studentInfo);
    },
    render: function () {

        return (
            <div>
                <h4>SignUp as Student</h4>
                <form name="StudentRegistration" onSubmit={this.handelValues}>
                    <hr/>
                    <table cellPadding="2" width="50%" bgcolor="99FFFF" align="center"
                           cellSpacing="2">
                        <tbody>
                        <tr>
                            <td>Name</td>
                            <td><input ref="studentName" required type='text' name='textnames' id="textname" size="30"/>
                            </td>
                        </tr>

                        <tr>
                            <td>EmailId</td>
                            <td><input ref="studentEmail" required type="email" name="emailid" id="emailid" size="30"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Password</td>
                            <td><input ref="studentPass" required type="password" name="pass" id="pass" size="30"/></td>
                        </tr>
                        <tr>
                            <td>Confirm Password</td>
                            <td><input required type="password" name="confirmpass" id="confirmpass"
                                       size="30"/></td>
                        </tr>

                        <tr>
                            <td>Father Name</td>
                            <td><input ref="studentFatherName" required type="text" name="fathername" id="fathername"
                                       size="30"/></td>
                        </tr>
                        <tr>
                            <td>Postal Address</td>
                            <td><input ref="studentPostalAddress" required type="text" name="paddress" id="paddress"
                                       size="30"/></td>
                        </tr>

                        <tr>
                            <td>Personal Address</td>
                            <td><input ref="studentAddress" required type="text" name="personaladdress"
                                       id="personaladdress" size="30"/></td>
                        </tr>

                        <tr onChange={this.setGender}>
                            <td>Gender</td>
                            <td><input type="radio" name="gender" value="male" size="10"/>Male
                                <input type="radio" name="gender" value="female" size="10"/>Female
                            </td>
                        </tr>

                        <tr>
                            <td>City</td>
                            <td><select onChange={this.setCity} name="City" defaultValue='-1'>
                                <option value="-1">select..</option>
                                <option value="Karachi">Karachi</option>
                                <option value="Islamabad">Islamabad</option>
                                <option value="Qetta">Qetta</option>
                                <option value="Peshawar">Peshawar</option>
                                <option value="Lahore">Lahore</option>
                            </select></td>
                        </tr>

                        <tr>
                            <td >Course</td>
                            <td><select onChange={this.setCourse} name="Course" defaultValue="-1">
                                <option value="-1">select..</option>
                                <option value="BS">BS</option>
                                <option value="MS">MS</option>
                                <option value="MPhil">MPhil</option>
                            </select></td>
                        </tr>

                        <tr>
                            <td>Current CGPA</td>
                            <td><input ref="cgpa" required min="0" max="4" type="number" name="cgpa" id="cgpa"
                                       size="30"/></td>
                        </tr>

                        <tr>
                            <td>DOB</td>
                            <td><input ref="dob" required type="date" name="dob" id="dob" size="30"/></td>
                        </tr>

                        <tr>
                            <td>MobileNo</td>
                            <td><input ref="mobNo" required type="number
" name="mobileno" id="mobileno" size="30"/></td>
                        </tr>
                        <tr>
                            <td><input type="reset"/></td>
                        </tr>
                        </tbody>
                    </table>
                    <button onClick={this.handelValues}>Submit</button>
                </form>

            </div>
        );
    },
});

function mapStateToProps(state) {

    return {};
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        AddStudentToFirebase: actions.AddStudentToDatabase,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(StudentSignupForm);