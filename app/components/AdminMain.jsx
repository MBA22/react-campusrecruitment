var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var AdminMain = React.createClass({

    componentDidMount: function () {
        this.props.getDataForAdmin();
    },
    deleteStudent: function (key) {
        this.props.deleteStudent(key);
    },
    deleteCompany: function (key) {
        this.props.deleteCompany(key);
    },
    render: function () {

        if (this.props.currentProps.fetching) {
            return <h5>Loading.....</h5>;
        }
        else {
            console.log(this.props.currentProps);
            var students = this.props.currentProps.students;
            var companies = this.props.currentProps.companies;
            return (
                <div>
                    <h1 className="text-center">Admin Panel</h1>

                    <h2>Companies</h2>

                    {
                        Object.keys(companies).map(function (key) {
                            return (
                                <div className="well col-xs-4">
                                    <h3 className="text-center">Name:{companies[key].name}</h3>
                                    <li className="list-item-group">Email:{companies[key].email}</li>
                                    <li className="list-item-group">Number:{companies[key].mobNo}</li>
                                    <button className="btn btn-danger" onClick={this.deleteCompany.bind(this, key)}>
                                        Delete
                                    </button>
                                </div>
                            );
                        }.bind(this))}

                    <h2>Students</h2>

                    {
                        Object.keys(students).map(function (key) {
                            return (
                                <div className="well col-xs-4">
                                    <h3>Name:{students[key].name}</h3>
                                    <li className="list-item-group">Email{students[key].email}</li>
                                    <li className="list-item-group">Number:{students[key].mobNo}</li>
                                    <li className="list-item-group">CGPA:{students[key].cgpa}</li>
                                    <li className="list-item-group">Degree:{students[key].course}</li>
                                    <button className="btn btn-danger" onClick={this.deleteStudent.bind(this, key)}>
                                        Delete
                                    </button>
                                </div>
                            );
                        }.bind(this))}


                </div>
            );
        }
    },
});


function mapStateToProps(state) {

    return {
        currentProps: state.getStudentsCompanies,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDataForAdmin: actions.getDataForAdmin,
        deleteCompany: actions.deleteCompany,
        deleteStudent: actions.deleteStudent,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(AdminMain);