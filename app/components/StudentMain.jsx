var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var StudentMain = React.createClass({

    componentDidMount: function () {
        console.log("in componentDidMount");
        this.props.getStudents();
        this.props.getCompanies();
    },
    render: function () {

        if (this.props.currentProps.fetching) {
            return <h5>Loading.....</h5>;
        }
        else {
            var companies = this.props.currentProps.companies;
            console.log('in students Component', this.props.currentProps.companies);
            return (
                <div>
                    <h1 className="text-center">Student Info</h1>
                    <h2>{this.props.currentProps.student.name}</h2>
                    <div className="row">
                        <div className="well">
                            <ul className="list-group">
                                <li className="list-group-item">Father
                                    Name:{this.props.currentProps.student.fatherName}</li>
                                <li className="list-group-item">Email:{this.props.currentProps.student.email}</li>
                                <li className="list-group-item">Gender:{this.props.currentProps.student.gender}</li>
                                <li className="list-group-item">Gender:{this.props.currentProps.student.gender}</li>
                                <li className="list-group-item">DOB:{this.props.currentProps.student.dob}</li>
                                <li className="list-group-item">Degree:{this.props.currentProps.student.course}</li>
                                <li className="list-group-item">Address:{this.props.currentProps.student.address}</li>
                                <li className="list-group-item">Postal
                                    Address:{this.props.currentProps.student.studentPostalAddress}</li>
                                <li className="list-group-item">City:{this.props.currentProps.student.city}</li>
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <h2>Companies</h2>
                        <div className="well">

                            {
                                Object.keys(companies).map(function (key) {
                                    var posts = companies[key].posts;
                                    if (posts != undefined || posts != null) {
                                        console.log("Posts", posts['sdfrqw3rq2e']);
                                        return (
                                            Object.keys(posts).map(function (postKey) {
                                                    return (
                                                        <div className="well col-xs-4" key={key}>
                                                            <h3>Company:{companies[key].name}</h3>
                                                            <h4>Company Number: {companies[key].mobNo}
                                                            </h4>
                                                            <h4>Company Email:{companies[key].email}</h4>
                                                            <h4>Company Vacancies:{posts[postKey]}</h4>
                                                            <br/>
                                                        </div>
                                                    );
                                                }
                                            ));
                                    } else {
                                        return (<div className="well col-xs-4" key={key}>

                                                <h3 >Company:{companies[key].name}</h3>
                                                <h4>Company Number: {companies[key].mobNo}</h4>
                                                <h4>Company Email:{companies[key].email}</h4><br/><br/>
                                            </div>
                                        );
                                    }
                                }.bind(this))
                            }
                        </div>
                    </div>
                </div>
            );
        }
    },
});


function mapStateToProps(state) {

    return {
        currentProps: state.getStudentsCompanies,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getStudents: actions.getStudents,
        getCompanies: actions.getCompaniesPosts,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(StudentMain);