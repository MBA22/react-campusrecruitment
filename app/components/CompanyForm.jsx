var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var companyInfo = {};
var signUp = {};
var CompanySignupForm = React.createClass({
    setGender: function (e) {
        var gender = e.target.value;
        companyInfo.gender = gender;
    },
    setCity: function (e) {
        var city = e.target.value;
        console.log(city);
        companyInfo.city = city;
    },
    setCourse: function (e) {
        var course = e.target.value;
        companyInfo.course = course;
    },

    handelValues: function (e) {
        e.preventDefault();
        companyInfo.name = this.refs.companyName.value;
        companyInfo.email = this.refs.companyEmail.value;
        signUp.email = this.refs.companyEmail.value;
        signUp.password = this.refs.companyPass.value;
        companyInfo.companyAddress = this.refs.companyAddress.value;
        companyInfo.mobNo = this.refs.mobNo.value;

        this.props.AddCompanyToFirebase(signUp, companyInfo);
    },
    render: function () {

        return (
            <div>
                <h4>SignUp as Company</h4>
                <form name="StudentRegistration" onSubmit={this.handelValues}>
                    <hr/>
                    <table cellPadding="2" width="50%" bgcolor="99FFFF" align="center"
                           cellSpacing="2">
                        <tbody>
                        <tr>
                            <td>Company Name</td>
                            <td><input ref="companyName" required type='text' name='textnames' id="textname" size="30"/>
                            </td>
                        </tr>

                        <tr>
                            <td>EmailId</td>
                            <td><input ref="companyEmail" required type="email" name="emailid" id="emailid" size="30"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Password</td>
                            <td><input ref="companyPass" required type="password" name="pass" id="pass" size="30"/></td>
                        </tr>
                        <tr>
                            <td>Confirm Password</td>
                            <td><input required type="password" name="confirmpass" id="confirmpass"
                                       size="30"/></td>
                        </tr>

                        <tr>
                            <td>Address</td>
                            <td><input ref="companyAddress" required type="text" name="personaladdress"
                                       id="personaladdress" size="30"/></td>
                        </tr>


                        <tr>
                            <td>City</td>
                            <td><select onChange={this.setCity} name="City" defaultValue='-1'>
                                <option value="-1">select..</option>
                                <option value="Karachi">Karachi</option>
                                <option value="Islamabad">Islamabad</option>
                                <option value="Qetta">Qetta</option>
                                <option value="Peshawar">Peshawar</option>
                                <option value="Lahore">Lahore</option>
                            </select></td>
                        </tr>

                        <tr>
                            <td>Office No</td>
                            <td><input ref="mobNo" required type="number" name="officeno" id="officeno" size="30"/></td>
                        </tr>
                        <tr>
                            <td><input className="btn btn-info" type="reset"/></td>
                        </tr>
                        </tbody>
                    </table>
                    <button className="btn btn-success" onClick={this.handelValues}>Submit</button>
                </form>

            </div>
        );
    },
});

function mapStateToProps(state) {

    return {};
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        AddCompanyToFirebase: actions.AddCompanyToDatabase,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(CompanySignupForm);