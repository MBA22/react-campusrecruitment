var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var StudentSignupForm = require('Studentform');
var CompanySignupForm = require('Companyform');

var Login = React.createClass({

    changeToSignUp: function (signup) {
        this.props.loginOrSignup(signup);
    },

    loginToAccount: function () {
        this.props.loginStart(this.refs.email.value, this.refs.password.value, this.refs.role.value);
    },
    render: function () {
        console.log('props at render', this.props.loginorSignin);
        if (this.props.loginorSignin.isSigning == 'studentSignUp') {
            return (
                <StudentSignupForm></StudentSignupForm>
            );
        }
        else if (this.props.loginorSignin.isSigning == 'companySignUp') {
            return (
                <CompanySignupForm></CompanySignupForm>
            );
        }
        else
            return (
                <div>
                    <h1 className="text-center">Login</h1>
                    <form>
                        <div className="form-group">
                            <input className="form-control" ref="email" type="text" placeholder="your email"/>
                        </div>
                        <div className="form-group">
                            <input className="form-control" ref="password" type="password" placeholder="password"/>
                        </div>
                        <div className="form-group">
                            <select className="form-control" ref="role" required defaultValue="student">
                                <option value="admin">Admin</option>
                                <option value="student">Student</option>
                                <option value="company">Company</option>
                            </select>
                        </div>
                    </form>
                    <button className="btn btn-primary" onClick={this.loginToAccount}>Login</button>
                    <br/>
                    or
                    <br/>
                    <button className="btn btn-primary" onClick={this.changeToSignUp.bind(this, 'studentSignUp')}>SignUp
                        as Student
                    </button>
                    <br/>
                    <button className="btn btn-primary" onClick={this.changeToSignUp.bind(this, 'companySignUp')}>SignUp
                        as Company
                    </button>


                </div>
            );
    },
});

function mapStateToProps(state) {

    return {
        loginorSignin: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        loginOrSignup: actions.loginOrSignUp,
        loginStart: actions.loginStart,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Login);