var React = require("react");
var Company = require('Company');
var AdminMain = require('Adminmain');
var StudentMain = require('Studentmain');

var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var Main = React.createClass({
    render: function () {
        console.log("in render of Main COmp", this.props.currentProps)
        if (this.props.currentProps.role === 'student') {
            return (
                <div className="container">
                    <button className="btn btn-danger" onClick={this.props.removeUid}>Logout</button>
                    <StudentMain></StudentMain>
                </div>
            );
        }
        else if (this.props.currentProps.role === 'company') {
            return (
                <div className="container">
                    <button className="btn btn-danger" onClick={this.props.removeUid}>Logout</button>
                    <Company></Company>
                </div>
            );
        }
        else if (this.props.currentProps.role === 'admin') {
            return (
                <div className="container">
                    <button className="btn btn-danger" onClick={this.props.removeUid}>Logout</button>
                    <AdminMain></AdminMain>
                </div>
            );
        }
        else
            return (
                <div className="container">
                    <h5 className="text-center">Not authorized</h5>
                    <button className="btn btn-danger" onClick={this.props.removeUid}>Return to Login</button>
                </div>
            );
    },
});

function mapStateToProps(state) {

    return {
        currentProps: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        removeUid: actions.logoutStart,
    }, dispatch);
}
module.exports = connect(mapStateToProps, matchDispatchToProps)(Main);