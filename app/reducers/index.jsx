export var loginsignupReducer = function (state = {}, action) {

    switch (action.type) {

        case "ISLOGINGORSIGNUP":
            var newState = Object.assign({}, state, {isSigning: action.isSinging});
            return newState;

        case 'LOGIN':
            console.log("In LOGINLOGOUTREDUCER", action.uid);
            var newState = Object.assign({}, state, {uid: action.uid});
            return newState;

        case "LOGINCHECK":
            console.log("at reducer", action.role);
            var newState = Object.assign({}, state, {role: action.role});
            return newState;

        case 'LOGOUT':
            return {};
        default:
            return state;
    }
};

export var getStudentsCompanies = function (state = {}, action) {
    switch (action.type) {
        case 'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'DONE_FETCHINGSTUDENT':
            var newState = Object.assign({}, state, {student: action.student});
            return newState;

        case 'DONE_FETCHINGCOMPANIES':
            var newState = Object.assign({}, state, {fetching: false, companies: action.companies});
            return newState;

        case 'DONE_FETCHINGCOMPANIES_ADMIN':
            var newState = Object.assign({}, state, {companies: action.companies});
            return newState;

        case 'DONE_FETCHINGCOMPANY':
            var newState = Object.assign({}, state, {company: action.company});
            return newState;

        case 'DONE_FETCHINGSTUDENTS':
            var newState = Object.assign({}, state, {fetching: false, students: action.students});
            return newState;

        case 'DONE_FETCHING':
            var newState = Object.assign({}, state, {fetching: false});
            return newState;

        case 'ADDED_POST':
            var newState = Object.assign({}, state, {fetching: false, posts: action.post});
            return newState;

        default:
            return state;
    }

};